import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df



def get_filled(df):
    def extract_title(name):
        for title in ["Mr.", "Mrs.", "Miss."]:
            if title in name:
                return title
        return "Other"

    df['Title'] = df['Name'].apply(extract_title)

    results = []

    for title in ["Mr.", "Mrs.", "Miss."]:
        group = df[df['Title'] == title]
        missing_values = group['Age'].isnull().sum()
        median_value = round(group['Age'].median())
        results.append((title, missing_values, median_value))

    return results


print(get_filled(get_titatic_dataframe()))